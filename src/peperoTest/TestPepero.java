package peperoTest;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

import org.junit.Test;
import org.junit.Before;

import pepero.Bordador;
import pepero.CentroDeProcesamiento;
import pepero.Cliente;
import pepero.Color;
import pepero.Costurero;
import pepero.Deposito;
import pepero.Fabrica;
import pepero.Material;
import pepero.Oficina;
import pepero.Remendon;
import pepero.TintoreriaGrosa;
import pepero.TintoreriaNormal;
import pepero.Uniforme;

public class TestPepero {
	Material lino;

	@Before
	public void setUp() {
		lino = new Material("lino", 4, 6);
	}

	// 1. SABER EL GRADO DE RESISTENCIA DE UN UNIFORME
	@Test //1
	public void resistenciaUniforme(){
		Fabrica fabrica1 = new Fabrica(10.00, "Jose", Color.ROJO, 100);
		Uniforme uniforme1 = new Uniforme (Color.ROJO,2 ,5, 4, lino, fabrica1);
		assertEquals (5.8, uniforme1.getGradoResistenciaUniforme(), 0.1);
	}
 
	
	// 2. SABER SI UN TALLER PUEDE TRABAJAR CON UN UNIFORME..
	@Test //2
	public void puedeTrabajarConUniformeTintoreriaNormal(){
		Material algodon = new Material("material", 9, 2);
		Deposito deposito1 = new Deposito ("Maria", Color.AZUL, 50);
		Uniforme uniforme2 = new Uniforme (Color.AZUL, 8, 12, 6, algodon, deposito1);
		TintoreriaNormal tintoreriaNormal1 = new TintoreriaNormal();
		tintoreriaNormal1.agregarMaterial(algodon);
		tintoreriaNormal1.agregarColor(Color.AZUL);
		assertTrue (tintoreriaNormal1.puedeTrabajarConUniforme(uniforme2));
	}
	
	@Test //3
	public void puedeTrabajarConUniformeTintoreriaGrosa(){
		Material demin = new Material("demin", 18, 20);
		Oficina oficina1 = new Oficina("Luis", Color.AZUL, 1000);
		Uniforme uniforme3 = new Uniforme (Color.MARRON, 54, 20, 11, demin, oficina1);
		TintoreriaGrosa tintoreriaGrosa1 = new TintoreriaGrosa();
		assertTrue (tintoreriaGrosa1.puedeTrabajarConUniforme(uniforme3));
	}
	
	@Test //4
	public void puedeTrabajarConUniformeCosturero(){
		Material grafa = new Material ("grafa", 15, 35);
		Deposito deposito2 = new Deposito ("Juana", Color.AZUL, 150);
		Uniforme uniforme4 = new Uniforme (Color.AZUL, 44, 1, 7, grafa, deposito2);
		Costurero costurero1 = new Costurero();
		assertTrue (costurero1.puedeTrabajarConUniforme(uniforme4));
	}
	
	@Test //5
	public void puedeTrabajarConUniformeRemendon(){
		Material acetato = new Material("acetato", 20, 40);
		Fabrica fabrica2 = new Fabrica(8.11, "Pedro", Color.ROJO, 200);
		Uniforme uniforme5 = new Uniforme (Color.ROJO, 3, 2, 1, acetato, fabrica2);
		Remendon remendon1 = new Remendon();
		assertFalse (remendon1.puedeTrabajarConUniforme(uniforme5));
	}
	
	@Test //6
	public void puedeTrabajarConUniformeBordador(){
		Material jersey = new Material("jersey", 5, 10);
		Oficina oficina2 = new Oficina("Rogelio", Color.MARRON, 2000);
		Uniforme uniforme6 = new Uniforme (Color.NEGRO, 2, 4, 6, jersey, oficina2);
		Bordador bordador1 = new Bordador();
		assertTrue (bordador1.puedeTrabajarConUniforme(uniforme6));
	}
	
	// SABER SI UN UNIFORME, RESPECTO DEL CLIENTE AL QUE ESTA ASIGNADO
	// a) ES DE UN COLOR ACEPTADO POR EL CLIENTE 
	@Test //7
	public void colorAceptadoPorFabrica(){
		Material lino = new Material("lino", 4, 6);
		Fabrica fabrica3 = new Fabrica(6.22, "Carolina", Color.ROJO, 400);
		Uniforme uniforme1 = new Uniforme (Color.ROJO, 2 ,5, 4, lino, fabrica3);
		Fabrica fabrica1 = new Fabrica(20.00, "La Fabrica", Color.ROJO, 500);
		assertTrue (fabrica1.leGustaElColor(uniforme1));
		}
	
	@Test //8
	public void colorAceptadoPorDeposito(){
		Material algodon = new Material("material", 9, 2);
		Deposito deposito3 = new Deposito ("Carla", Color.VERDE, 250);
		Uniforme uniforme2 = new Uniforme (Color.VERDE, 8, 12, 6, algodon, deposito3);
		Deposito deposito1 = new Deposito ("El Deposito", Color.VERDE, 350);
		assertFalse (deposito1.leGustaElColor(uniforme2));
	}
	
	@Test //9
	public void colorAceptadoPorOficina(){
		Material demin = new Material("demin", 18, 20);
		Oficina oficina3 = new Oficina ("Santiago", Color.NEGRO, 3000);
		Uniforme uniforme3 = new Uniforme (Color.MARRON, 54, 20, 11, demin, oficina3);
		Oficina oficina1 = new Oficina ("La Oficina", Color.NEGRO, 4000);
		assertFalse (oficina1.leGustaElColor(uniforme3));
	}
	
	//b) SU GRADO DE RESISTENCIA ES SUFICIENTE
	@Test //10
	public void resistenciaSuficienteFabrica(){
		Material grafa = new Material ("grafa", 15, 35);
		Fabrica fabrica4 = new Fabrica (45.00, "Lujan", Color.AZUL, 600);
		Uniforme uniforme4 = new Uniforme (Color.AZUL, 44, 1, 7, grafa, fabrica4);
		assertFalse (fabrica4.suResistenciaEsSuficiente(uniforme4));
		//assertTrue (uniforme4.getResistenciaUniforme() > fabrica1.getResistenciaFabrica());//cambiar
	}
	
	@Test //11
	public void resistenciaSuficienteDeposito(){
		Material acetato = new Material("acetato", 20, 40);
		Deposito deposito4 = new Deposito ("Enrique", Color.ROJO, 450);
		Uniforme uniforme5 = new Uniforme (Color.ROJO, 3, 2, 1, acetato, deposito4);
		assertTrue (deposito4.suResistenciaEsSuficiente(uniforme5));
		//assertFalse (uniforme5.getResistenciaUniforme() < deposito1.getResistenciaDeposito());//cambiar
	}
	
	@Test //12
	public void resistenciaSuficienteOficina() {
		Material jersey = new Material("jersey", 5, 10);
		Oficina oficina4 = new Oficina ("Cecilia", Color.VERDE, 5000);
		Uniforme uniforme6 = new Uniforme (Color.NEGRO, 2, 4, 6, jersey, oficina4);
		Oficina oficina1 = new Oficina ("El Cubo S.R.L.", Color.VERDE, 6000);
		assertTrue (oficina1.suResistenciaEsSuficiente(uniforme6));
	}
	
	
	//c) LA LEYENDA QUE TIENE ESTAMPADA ES CORRECTA
	@Test //13
	public void leyendaCorrectaFabrica() {
		Material gabardina = new Material ("gabardina", 6, 12);
		Fabrica fabrica2 = new Fabrica (11.11, "Fabricon", Color.MARRON, 900);
		Uniforme uniforme7 = new Uniforme (Color.MARRON, 5, 10, 15, gabardina, fabrica2);
		uniforme7.setLeyenda("Fabricon");
		assertTrue(fabrica2.leyendaCorrecta(uniforme7));
	}
	
	//d) SI ESTA LISTO. SI LAS 3 COSAS ESTAN OK.
	@Test //14
	public void uniformeOkFabrica(){
		Material jersey = new Material("jersey", 5, 10);
		Fabrica fabrica12 = new Fabrica (9.00, "FabricaMala", Color.NEGRO, 900);
		Uniforme uniforme6 = new Uniforme (Color.NEGRO, 8, 4, 6, jersey, fabrica12);
		uniforme6.setLeyenda("FabricaMala");
		assertTrue(uniforme6.uniformeOk());
	}
	
	@Test //15
	public void uniformeOkDeposito(){
		Material lino = new Material("lino", 4, 6);
		Deposito deposito3 = new Deposito ("Carla", Color.NEGRO, 250);
		Uniforme uniforme1 = new Uniforme (Color.NEGRO,2 ,5, 44, lino, deposito3);
		uniforme1.setLeyenda("Carla");
		assertTrue(uniforme1.uniformeOk());
	}
	
	@Test //16
	public void uniformeOkOficina(){
		Material jersey = new Material("jersey", 5, 10);
		Oficina oficina1 = new Oficina ("La Oficina", Color.NEGRO, 4000);
		Uniforme uniforme7 = new Uniforme (Color.NEGRO, 5, 10, 15, jersey, oficina1);
		uniforme7.setLeyenda("La Oficina");
		assertTrue(uniforme7.uniformeOk());
	}
	
	@Test //17
	public void uniformeNoOkFabrica(){
		Material jersey = new Material("jersey", 5, 10);
		Fabrica fabrica2 = new Fabrica (11.11, "FabricaMala", Color.NEGRO, 900);
		Uniforme uniforme6 = new Uniforme (Color.NEGRO, 2, 4, 6, jersey, fabrica2);
		assertTrue(uniforme6.uniformeNoOk());
	}
	
	@Test //18
	public void uniformeNoOkDeposito(){
		Material lino = new Material("lino", 4, 6);
		Deposito deposito3 = new Deposito ("Carla", Color.ROJO, 250);
		Uniforme uniforme1 = new Uniforme (Color.ROJO,2 ,5, 4, lino, deposito3);
		assertTrue(uniforme1.uniformeNoOk());
	}
	
	@Test //19
	public void uniformeNoOkOficina(){
		Material jersey = new Material("jersey", 5, 10);
		Oficina oficina1 = new Oficina ("La Oficina", Color.NEGRO, 4000);
		Uniforme uniforme7 = new Uniforme (Color.MARRON, 5, 10, 15, jersey, oficina1);
		assertTrue(uniforme7.uniformeNoOk());
	}
	
	
	//4) a) LOS CLIENTES PARA LOS QUE TIENE UNIFORMES LISTOS. SIN REPETIDOS.
	@Test //20
	public void clientesQueTienenUniformesListos() {
		CentroDeProcesamiento nro1 = new CentroDeProcesamiento();
		Material lino = new Material("lino", 4, 6);
		Fabrica fabrica1 = new Fabrica(10.00, "Jose", Color.ROJO, 100);
		Uniforme uniforme85 = new Uniforme (Color.ROJO, 20 ,55, 40, lino, fabrica1);
		uniforme85.setLeyenda("Jose");
		nro1.agregarUniforme(uniforme85);
		Material jersey = new Material("jersey", 5, 10);
		Oficina oficina2 = new Oficina("Rogelio", Color.NEGRO, 2000);
		Uniforme uniforme33 = new Uniforme (Color.NEGRO, 2, 4, 6, jersey, oficina2);
		uniforme33.setLeyenda("Rogelio");
		nro1.agregarUniforme(uniforme33);
		Collection<Cliente> clientes = new HashSet<>();
		clientes.add(fabrica1);
		clientes.add(oficina2);
		assertEquals(clientes, nro1.clientesDeUniformesOk());	
	}
	
	
	//4) b) CUANTOS UNIFORMES LISTOS TIENE 
	@Test //21
	public void cuantosUniformesListos() {
		CentroDeProcesamiento nro1 = new CentroDeProcesamiento();
		Material lino = new Material("lino", 4, 6);
		Fabrica fabrica1 = new Fabrica(10.00, "Jose", Color.ROJO, 100);
		Uniforme uniforme23 = new Uniforme (Color.ROJO,25 ,50, 40, lino, fabrica1);
		uniforme23.setLeyenda("Jose");
		nro1.agregarUniforme(uniforme23);	
		Material jersey = new Material("jersey", 5, 10);
		Oficina oficina2 = new Oficina("Rogelio", Color.MARRON, 2000);
		Uniforme uniforme44 = new Uniforme (Color.MARRON, 2, 4, 6, jersey, oficina2); 
		uniforme44.setLeyenda("Rogelio");
		nro1.agregarUniforme(uniforme44);
		assertEquals(2, nro1.cantidadUniformesOk());
	}

	//4) c) SABER EL VALOR TOTAL DE LOS UNIFORMES 
	@Test //22
	public void valorDeLosUniformesListos() {
		CentroDeProcesamiento nro2 = new CentroDeProcesamiento();
		Material lino = new Material("lino", 4, 6);
		Fabrica fabrica1 = new Fabrica(10.00, "Jose", Color.ROJO, 100);
		Uniforme uniforme35 = new Uniforme (Color.ROJO, 20 ,55, 45, lino, fabrica1);
		uniforme35.setLeyenda("Jose");
		nro2.agregarUniforme(uniforme35);
		Material jersey = new Material("jersey", 5, 10);
		Oficina oficina2 = new Oficina("Rogelio", Color.MARRON, 2000);
		Uniforme uniforme50 = new Uniforme (Color.MARRON, 2, 4, 6, jersey, oficina2);
		uniforme50.setLeyenda("Rogelio");
		nro2.agregarUniforme(uniforme50);
		assertEquals(2100, nro2.valorTotalUniformesListos());
	}
	
	//5-A SACAR UNIFORMES LISTOS DEL CENTRO DE PROCESAMIENTO
	@Test //23
	public void sacarDelCDPUniformesListosDeUnCliente() {
		CentroDeProcesamiento nro3 = new CentroDeProcesamiento();
		Material lino = new Material("lino", 4, 6);
		Fabrica fabrica1 = new Fabrica(10.00, "Jose", Color.ROJO, 100);
		Uniforme uniforme35 = new Uniforme (Color.ROJO, 20 ,55, 45, lino, fabrica1);
		uniforme35.setLeyenda("Jose");
		nro3.agregarUniforme(uniforme35);
		Material jersey = new Material("jersey", 5, 10);
		Oficina oficina2 = new Oficina("Rogelio", Color.MARRON, 2000);
		Uniforme uniforme50 = new Uniforme (Color.MARRON, 2, 4, 6, jersey, oficina2);
		uniforme50.setLeyenda("Rogelio");
		nro3.agregarUniforme(uniforme50);
		Material gabardina = new Material ("gabardina", 6, 12);
		Fabrica fabrica2 = new Fabrica (111.11, "Fabricon", Color.MARRON, 900);
		Uniforme uniforme7 = new Uniforme (Color.MARRON, 5, 10, 15, gabardina, fabrica2);
		uniforme7.setLeyenda("Fabricon");
		nro3.agregarUniforme(uniforme7);
		assertEquals(
			new HashSet<>(Arrays.asList(uniforme7, uniforme50)), 
			nro3.uniformesSinListosParaCliente(fabrica1));
	}
	
	//5-A AGREGAR UNIFORMES LISTOS AL CLIENTE
	@Test //24
	public void agregarUniformesListosAlCliente() {
		Fabrica cliente1 = new Fabrica (10.00, "cliente1", Color.AZUL, 20);
		Material lino = new Material("lino", 4, 6);
		Uniforme uniforme35 = new Uniforme (Color.ROJO, 20 ,55, 45, lino, cliente1);
		uniforme35.setLeyenda("cliente1");
		cliente1.agregarUniformeListo(uniforme35);
		Material jersey = new Material("jersey", 5, 10);
		Uniforme uniforme50 = new Uniforme (Color.MARRON, 2, 4, 6, jersey, cliente1);
		uniforme50.setLeyenda("cliente1");
		cliente1.agregarUniformeListo(uniforme50);
		Material gabardina = new Material ("gabardina", 6, 1);
		Uniforme uniforme7 = new Uniforme (Color.MARRON, 5, 10, 15, gabardina, cliente1);
		uniforme7.setLeyenda("cliente1");
		cliente1.agregarUniformeListo(uniforme7);
		assertEquals(
				new HashSet<>(Arrays.asList(uniforme35,uniforme50,uniforme7)),
				cliente1.getUniformesListos());	
	}
	
	//5-A bis PASAR UNIFORMES LISTOS DE CENTRO A CLIENTE 
	@Test //25
	public void pasarUniformesListosDeCentroACliente() {
		CentroDeProcesamiento nro4 = new CentroDeProcesamiento();
		
		Fabrica cliente1 = new Fabrica (10.00, "cliente1", Color.AZUL, 20);
		
		Material lino = new Material("lino", 4, 6);
		Uniforme uniforme35 = new Uniforme (Color.ROJO, 20 ,55, 45, lino, cliente1);
		uniforme35.setLeyenda("cliente1");
		nro4.agregarUniforme(uniforme35);
		
		Material jersey = new Material("jersey", 5, 10);
		Uniforme uniforme50 = new Uniforme (Color.MARRON, 2, 4, 6, jersey, cliente1);
		uniforme50.setLeyenda("cliente1");
		nro4.agregarUniforme(uniforme50);
		
		Material gabardina = new Material ("gabardina", 6, 1);
		Uniforme uniforme7 = new Uniforme (Color.MARRON, 5, 10, 15, gabardina, cliente1);
		uniforme7.setLeyenda("cliente1");
		nro4.agregarUniforme(uniforme7);
		
		nro4.pasarUniformesListosACliente(cliente1);
		
		assertEquals(
				new HashSet<>(Arrays.asList(uniforme7,uniforme50,uniforme35)),
				cliente1.getUniformesListos());
		
		
	}
	
	//5-B VALOR TOTAL DE LOS UNIFORMES QUE RECIBIO EL CLIENTE
	@Test //26
	public void valorTotalDeUniformesQueRecibioElCliente() {
		Fabrica cliente2 = new Fabrica (10.00, "cliente2", Color.AZUL, 20);
		Material lino = new Material("lino", 4, 6);
		Uniforme uniforme35 = new Uniforme (Color.ROJO, 20 ,55, 45, lino, cliente2);
		uniforme35.setLeyenda("cliente2");
		cliente2.agregarUniformeListo(uniforme35);
		Material jersey = new Material("jersey", 5, 10);
		Uniforme uniforme50 = new Uniforme (Color.MARRON, 2, 4, 6, jersey, cliente2);
		uniforme50.setLeyenda("cliente2");
		cliente2.agregarUniformeListo(uniforme50);
		Material gabardina = new Material ("gabardina", 6, 1);
		Uniforme uniforme7 = new Uniforme (Color.MARRON, 5, 10, 15, gabardina, cliente2);
		uniforme7.setLeyenda("cliente2");
		cliente2.agregarUniformeListo(uniforme7);
		assertEquals(60, cliente2.valorTotalUniformesRecibidos());
	}
	
	//6-A LAS TINTORERIAS HACEN LO QUE TIENE QUE HACER
	@Test //27
	public void laTintoreriaHaceLoQueTieneQueHacer() {
		Material lino = new Material("lino", 4, 6);
		Cliente deposito3 = new Deposito ("Carla", Color.NEGRO, 250);
		Uniforme uniforme1 = new Uniforme (Color.MARRON,2 ,5, 4, lino, deposito3);
		uniforme1.setCliente(deposito3);
		TintoreriaNormal tn1 = new TintoreriaNormal();
		tn1.agregarMaterial(lino);
		tn1.agregarColor(Color.MARRON);
		assertTrue(tn1.puedeTrabajarConUniforme(uniforme1));
		tn1.elTallerHaceLoqueTieneQueHacer(uniforme1);
		assertEquals(uniforme1.getColor(), Color.NEGRO);
	}
	
	//6-A EL COSTURERO HACE LO QUE TIENE QUE HACER
	@Test //28
	public void elCostureroHaceLoQueTieneQueHacer() {
		Material algodon = new Material("algodon", 5, 200);
		Cliente oficina5 = new Oficina ("oficina5", Color.AZUL, 15);
		Uniforme uniforme20 = new Uniforme (Color.NEGRO, 5, 10, 20, algodon, oficina5);
		uniforme20.setCliente(oficina5);
		Costurero costurero1 = new Costurero();
		assertTrue(costurero1.puedeTrabajarConUniforme(uniforme20));
		costurero1.elTallerHaceLoqueTieneQueHacer(uniforme20);
		assertEquals(uniforme20.getCosturasExtras(),21);
	}
	
	//6-A EL REMENDON HACE LO QUE TIENE QUE HACER 
	@Test //29
	public void elRemendonHaceLoQueTieneQueHacer() {
		Material grafa = new Material ("grafa", 20, 25);
		Cliente oficina7 = new Oficina ("oficina7", Color.ROJO, 12);
		Uniforme uniforme32 = new Uniforme (Color.VERDE, 10, 15, 20, grafa, oficina7);
		uniforme32.setCliente(oficina7);
		Remendon remendon1 = new Remendon();
		assertTrue(remendon1.puedeTrabajarConUniforme(uniforme32));
		remendon1.elTallerHaceLoqueTieneQueHacer(uniforme32);
		assertEquals(uniforme32.getRefuerzos(),11);
	}
	
	// 6-A LOS BORDADORES ESTAMPAN LA LEYENDA 
	@Test //30
	public void elBordadorEstampaLaLeyenda() {
		Material seda = new Material ("seda",20,60);
		Cliente deposito8 = new Deposito ("deposito8", Color.MARRON, 52);
		Uniforme uniforme60 = new Uniforme (Color.AZUL,15,20,25,seda,deposito8);
		Bordador bordador22 = new Bordador();
		uniforme60.setLeyenda("deposito8");
		assertTrue(bordador22.puedeTrabajarConUniforme(uniforme60));
		bordador22.elTallerHaceLoqueTieneQueHacer(uniforme60);
		assertEquals(uniforme60.getLeyenda(),"deposito8");
	}
	
	//6-B SABER PARA UN UNIFORME POR CUANTOS TALLERES PASO
	@Test //31
	public void cantidadDeTalleresPorLosQuePaso() {	
		Material grafa = new Material ("grafa", 20, 25);
		Cliente oficina7 = new Oficina ("oficina7", Color.ROJO, 12);
		Uniforme uniforme32 = new Uniforme (Color.VERDE, 10, 15, 20, grafa, oficina7);
		uniforme32.setCliente(oficina7);
		Remendon remendon1 = new Remendon();
		Costurero costurero1 = new Costurero();
		uniforme32.agregarTaller(remendon1);
		uniforme32.agregarTaller(costurero1);
		assertEquals(uniforme32.porCuantosTalleresPaso(),2);
		assertTrue(uniforme32.pasoPorElTaller(costurero1));
		assertEquals(uniforme32.elUltimoTaller(costurero1),costurero1);
	}
	
	//6-C SABER PARA UN CDP CON QUE TALLERES TRABAJO DE UN UNIFORME DE UN CLIENTE
	@Test //32
	public void talleresDeLosUniformesDeUnCliente() {
		Material grafa = new Material ("grafa", 20, 25);
		Cliente oficina5 = new Oficina ("oficina5", Color.ROJO, 12);
		Uniforme uniforme32 = new Uniforme (Color.VERDE, 10, 15, 20, grafa, oficina5);
		uniforme32.setCliente(oficina5);
		Material algodon = new Material("algodon", 5, 200);
		Uniforme uniforme20 = new Uniforme (Color.NEGRO, 5, 10, 20, algodon, oficina5);
		uniforme20.setCliente(oficina5);
		Remendon remendon1 = new Remendon();
		Costurero costurero1 = new Costurero();
		Costurero costurero2 = new Costurero();
		TintoreriaGrosa tG1 = new TintoreriaGrosa();
		uniforme32.agregarTaller(tG1);
		uniforme32.agregarTaller(costurero1);
		uniforme32.agregarTaller(remendon1);
		uniforme20.agregarTaller(tG1);
		uniforme20.agregarTaller(costurero2);
		CentroDeProcesamiento cDP2 = new CentroDeProcesamiento();
		cDP2.agregarUniforme(uniforme32);
		cDP2.agregarUniforme(uniforme20);
		assertEquals(new HashSet<>(Arrays.asList(tG1,costurero1,remendon1,tG1,costurero2)), 
		cDP2.talleresDeLosUniformesDeUnCliente(oficina5));
		}
	
	//6-C UNIFORMES QUE PASARON POR UN TALLER
	@Test //33
		public void uniformesQuePasaronPorElTaller() {
		Material grafa = new Material ("grafa", 20, 25);
		Cliente oficina5 = new Oficina ("oficina5", Color.ROJO, 12);
		Uniforme uniforme32 = new Uniforme (Color.VERDE, 10, 15, 20, grafa, oficina5);
		uniforme32.setCliente(oficina5);
		Material algodon = new Material("algodon", 5, 200);
		Uniforme uniforme20 = new Uniforme (Color.NEGRO, 5, 10, 20, algodon, oficina5);
		uniforme20.setCliente(oficina5);
		Remendon remendon1 = new Remendon();
		Costurero costurero1 = new Costurero();
		Costurero costurero2 = new Costurero();
		TintoreriaGrosa tG1 = new TintoreriaGrosa();
		uniforme32.agregarTaller(tG1);
		uniforme32.agregarTaller(costurero1);
		uniforme32.agregarTaller(remendon1);
		uniforme20.agregarTaller(tG1);
		uniforme20.agregarTaller(costurero1);
		CentroDeProcesamiento cdp20 = new CentroDeProcesamiento();
		cdp20.agregarTaller(tG1);
		cdp20.agregarTaller(costurero2);
		cdp20.agregarTaller(costurero1);
		cdp20.agregarTaller(remendon1);
		cdp20.agregarUniforme(uniforme20);
		cdp20.agregarUniforme(uniforme32);
		assertEquals(new HashSet<>(Arrays.asList(uniforme32,uniforme20)), 
		cdp20.uniformesQuePasaronPorEsteTaller(costurero1));
	}
		
	
}

