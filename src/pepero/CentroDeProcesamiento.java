package pepero;

import java.util.Collection;
import java.util.HashSet;
//import java.util.List;
import java.util.stream.Collectors;

//Pepero S.A. tiene varios centros de procesamiento, a los que les van llegando los uniformes a medio hacer. 
//Cada centro de procesamiento trabaja con un conjunto de talleres. 
//La planta donde se hacen los uniformes est� fuera de este modelo, 
//cada uniforme llega de alguna forma a un centro de procesamiento, 
//y el centro tiene la responsabilidad de llevarlo a los talleres que haga falta para que tenga todo lo que necesita el cliente, 
//y despu�s envi�rselo al cliente.
//Cuando se fabrica un uniforme, ya se sabe para qu� cliente es, al centro de procesamiento llega con el cliente ya asignado. 
//Cuando un uniforme est� listo, se entrega, o sea pasa de estar en el centro de procesamiento a tenerlo el cliente.


public class CentroDeProcesamiento {

	private Collection <Uniforme> uniformes = new HashSet<>();
	//private Collection <Uniforme> uniformesListos = new HashSet<>();
	private Collection <Taller> talleres = new HashSet<>();
	
	public void agregarUniforme(Uniforme _uniforme){
		uniformes.add(_uniforme);
	}
	
	public Collection <Uniforme> getUniformes(){
		return this.uniformes;
	}
	
	public void agregarTaller (Taller _taller){
		talleres.add(_taller);
	}
	
	public Collection <Taller> getTalleres(){
		return this.talleres;
	}
	
	// 4. a) LOS CLIENTES PARA LOS QUE TIENE UNIFORMES LISTOS, SIN REPETIDOS. 
	
	public Collection <Cliente> clientesDeUniformesOk() {
		return this.uniformes.stream()
				.filter(u -> u.uniformeOk())
				.map(u -> u.getCliente())
				.collect(Collectors.toSet());
	}
	
	// 4. b) CUANTOS UNIFORMES LISTOS TIENE.
	public Collection <Uniforme> uniformesOk(){
		return this.uniformes.stream()
				.filter(u -> u.uniformeOk())
				.collect(Collectors.toSet());
	}
	
	public int cantidadUniformesOk(){
		return uniformesOk().size();
	}
	
	// 4. c) EL VALOR TOTAL DE LOS UNIFORMES LISTOS..
	public int valorTotalUniformesListos(){
		return this.uniformesOk().stream()
				.mapToInt(v -> v.cliente.getValor())
				.sum();
	}
	
	//5. MANEJAR LAS ENTREGAS DE LOS UNIFORMES LISTOS A LOS CLIENTES
	//a) PERMITIR EL REGISTRO DEL ENVIO A UN CLIENTE DE TODOS LOS UNIFORMES LISTOS 
	//	 EN UN CENTRO DE PROCESAMIENTO QUE SON PARA �L. O SEA, TODOS ESTOS UNIFORMES 
	//	 SALEN DEL CENTRO, Y PASA A TENERLOS EL CLIENTE.
	
	
	public Collection <Uniforme> uniformesSinListosParaCliente(Cliente _cliente){
		return this.uniformes.stream()
			        .filter(ul -> !ul.uniformeOkPara(_cliente))
			      	.collect(Collectors.toSet());
	}
	
	public Collection <Uniforme> uniformesListosParaCliente(Cliente _cliente){
		return this.uniformes.stream()
			        .filter(ul -> ul.uniformeOkPara(_cliente))
			      	.collect(Collectors.toSet());
	}
	
	public void pasarUniformesListosACliente(Cliente _cliente) {
		Collection<Uniforme> uniformesAMover = this.uniformesListosParaCliente(_cliente); 
		// sacar
		this.uniformes.removeAll(uniformesListosParaCliente(_cliente));
		// poner
		_cliente.agregarUniformesListos(uniformesAMover);
	}
	
	public Collection <Uniforme> uniformesDelCliente(Cliente _cliente) {
		return this.uniformes.stream()
				.filter(uc -> uc.getCliente().equals(_cliente))
				.collect(Collectors.toSet());
	}
	
	public Collection <Taller> talleresDeLosUniformesDeUnCliente(Cliente _cliente){
		return this.uniformesDelCliente(_cliente).stream()
			.map(u -> u.talleresDelUniforme())
			.flatMap(u -> u.stream())
			.collect(Collectors.toSet());
	}
	
	public Collection <Uniforme> uniformesQuePasaronPorEsteTaller(Taller _taller){
		return this.uniformes.stream()
			.filter(u -> u.talleresDelUniforme().contains(_taller))
			.collect(Collectors.toSet());
	}
}

