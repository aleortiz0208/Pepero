package pepero;

public class Remendon extends Taller {

	@Override
	public boolean puedeTrabajarConUniforme(Uniforme uniforme) {
		return uniforme.getRefuerzos() < uniforme.getRefuerzosMaximos();
	}

	@Override
	public void elTallerHaceLoqueTieneQueHacer(Uniforme uniforme) {
		if (this.puedeTrabajarConUniforme(uniforme)){
			uniforme.setRefuerzos(uniforme.getRefuerzos() + 1); 
		} 		
	}

	@Override
	public Taller pasoPorElTaller() {
		return this;
	}

}
