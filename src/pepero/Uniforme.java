package pepero;

import java.util.ArrayList;
import java.util.List;

public class Uniforme{
	
	protected Color color;
	protected String leyenda;
	protected int refuerzos;		
	protected int refuerzosMaximos;	
	protected int costurasExtras; 	
	protected Material material;	
	protected Cliente cliente;		
	
	
	@Override
	public String toString() {
		return "Uniforme [color=" + color + ", refuerzos=" + refuerzos + ", material=" + material + "]";
	}

	public Uniforme (Color _color, int _refuerzos, int _refuerzosMaximos, int _costurasExtras,
		Material _material, Cliente _cliente){
		this.color = _color;
		this.refuerzos = _refuerzos;
		this.refuerzosMaximos = _refuerzosMaximos;
		this.costurasExtras = _costurasExtras;
		this.material = _material;
		this.cliente = _cliente;
	}
	
	public void setColor(Color _color){
		this.color = _color;
	}
	
	public Color getColor(){
		return this.color;
	}
	
	public void setLeyenda(String _leyenda){
		this.leyenda = _leyenda;
	}
	
	public String getLeyenda(){
		return leyenda;
	}
	
	public int getRefuerzos(){
		return this.refuerzos; 
	}
	
	public int getRefuerzosMaximos(){
		return this.refuerzosMaximos;
	}
	
	public void setRefuerzos(int _refuerzos) {
		this.refuerzos = _refuerzos;
	}
	
	public void setCosturasExtras(int _costurasExtras){
		this.costurasExtras = _costurasExtras;
	}
	
	public int getCosturasExtras(){
		return this.costurasExtras;
	}
	
	public Material getMaterial(){
		return this.material;
	}
	
	public Cliente getCliente(){
		return this.cliente;
	}
	
	//1- SABER EL GRADO DE RESISTENCIA DE UN UNIFORME 
	public double getGradoResistenciaUniforme(){ 
		return (material.resistenciaMaterial + 0.5*this.refuerzos + 0.2*this.costurasExtras);
	}
	
	//EL UNIFORME ESTA OK
	public boolean uniformeOk() {
		return cliente.leyendaCorrecta(this) && cliente.leGustaElColor(this) 
				&& cliente.suResistenciaEsSuficiente(this);
	}

	//public boolean uniformeOk(Cliente cliente) {
		//return cliente.leyendaCorrecta(this) && cliente.leGustaElColor(this) 
			//	&& cliente.suResistenciaEsSuficiente(this);
	//}
	
	public boolean uniformeNoOk() {
		return !cliente.leyendaCorrecta(this) || !cliente.leGustaElColor(this) 
				|| !cliente.suResistenciaEsSuficiente(this);
	}

	public boolean uniformeOkPara(Cliente _cliente) {
		return this.cliente.equals(_cliente);
	}

	public List <Taller> talleresDelUniforme = new ArrayList<>();
	
	public void agregarTaller (Taller _taller){
		this.talleresDelUniforme.add(_taller);
	}
	
	public List <Taller> talleresDelUniforme(){
		return this.talleresDelUniforme;
	}
	
	//7-B SABER PARA UN UNIFORME POR CUANTOS TALLERES PASO, 
	//SI PASO O NO POR UN DETERMINADO TALLER
	//CUAL FUE EL ULTIMO TALLER POR EL QUE PASO
	public int porCuantosTalleresPaso(){
		return this.talleresDelUniforme.size();
	}
	
	public boolean pasoPorElTaller(Taller _taller){
		return this.talleresDelUniforme.contains(_taller);
	}
	
	public Taller elUltimoTaller(Taller _taller){
		return this.talleresDelUniforme().get(this.talleresDelUniforme().size()-1);
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
}
