package pepero;

public class Fabrica extends Cliente {
	
	private double resistenciaFabrica;
	
	public Fabrica (double _resistenciaFabrica, String _nombreEmpresa, Color _color, int _valor){
		super(_nombreEmpresa, _color, _valor);
		this.resistenciaFabrica = _resistenciaFabrica;
	}

	public double getResistenciaFabrica(){
		return resistenciaFabrica;
	}
	
	//necesitan uniformes cuyo grado de resistencia supere un valor que establece cada fabrica.
	//no tiene pretenciones respecto del color 	
		
	@Override
	public boolean leGustaElColor(Uniforme uniforme) {
		return true;
	}
	
	@Override
	public boolean suResistenciaEsSuficiente(Uniforme uniforme) {
		return getResistenciaFabrica()<uniforme.getGradoResistenciaUniforme();
	}

	@Override
	public String elCliente() {
		return "Fabrica";
	}

	
}
    