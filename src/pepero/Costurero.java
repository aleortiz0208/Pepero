package pepero;

public class Costurero extends Taller {

	
	@Override
	public boolean puedeTrabajarConUniforme(Uniforme uniforme){	
		return (uniforme.getCosturasExtras() < uniforme.getMaterial().getCosturasExtrasMaximas());
	}

	@Override
	public void elTallerHaceLoqueTieneQueHacer(Uniforme uniforme) {
		if (this.puedeTrabajarConUniforme(uniforme)){
			uniforme.setCosturasExtras(uniforme.getCosturasExtras() + 1); 
		}
		
	}

	@Override
	public Taller pasoPorElTaller() {
		return this;
	}
	
}	


//pueden agregar una costura extra, cada uniforme puede soportar una cantidad maxima de costuras extra que depende del material 
//(ej: demin hasta 3)