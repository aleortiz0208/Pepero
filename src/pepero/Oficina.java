package pepero;

public class Oficina extends Cliente {
	
	public Oficina (String _nombreEmpresa, Color _color, int _valor){
		super(_nombreEmpresa, _color, _valor);
	}
	
	public Color getColor(){
		return this.color;
	}
	
	@Override
	public boolean leGustaElColor(Uniforme uniforme) {
		if (uniforme.getColor().equals(this.color)) {
			return true; 
		} else { return false; }
	}

	@Override
	public boolean suResistenciaEsSuficiente(Uniforme uniforme) {
		return true;
	}

	@Override
	public String elCliente() {
		return "Oficina";
	}

}

	//necesitan uniformes que sean de un color determinado, que determina cada oficina. 
	//	No tienen requerimientos sobre la resistencia	
		