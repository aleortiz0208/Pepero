package pepero;

public class TintoreriaGrosa extends Taller {

	@Override
	public boolean puedeTrabajarConUniforme(Uniforme uniforme) {
		return true;
	}

	@Override
	public void elTallerHaceLoqueTieneQueHacer(Uniforme uniforme) {
		if (this.puedeTrabajarConUniforme(uniforme)) {
			if (uniforme.cliente.elCliente().equals("Deposito")){
				uniforme.setColor(Color.NEGRO);
				}
			if (uniforme.cliente.elCliente().equals("Oficina")){
				uniforme.setColor(uniforme.cliente.getColor());
			}
		}	
	}

	@Override
	public Taller pasoPorElTaller() {
		return this;
	}
	
}
