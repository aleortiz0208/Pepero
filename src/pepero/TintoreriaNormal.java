package pepero;

import java.util.Collection;
import java.util.HashSet;

public class TintoreriaNormal extends Taller {
	
	private Collection <Material> materialesPermitidos = new HashSet<>();
	
	
	public void agregarMaterial(Material material){
		materialesPermitidos.add (material);
	}
	
	public Collection <Material> materialesPermitidos(){
		return this.materialesPermitidos;
	}
	
	public Collection <Color> coloresQuePuedeTransformar = new HashSet<>();
	
	public void agregarColor (Color color){
		coloresQuePuedeTransformar.add(color);
	}
	
	public Collection <Color> coloresQuePuedeTransformar(){
		return this.coloresQuePuedeTransformar;
	}
	
	@Override
	public boolean puedeTrabajarConUniforme(Uniforme uniforme){
		return coloresQuePuedeTransformar().contains(uniforme.getColor()) && materialesPermitidos().contains(uniforme.getMaterial());
	}

	@Override
	public void elTallerHaceLoqueTieneQueHacer(Uniforme uniforme) {
		if (this.puedeTrabajarConUniforme(uniforme)) {
			if (uniforme.cliente.elCliente().equals("Deposito")){
				uniforme.setColor(Color.NEGRO);
				}
			if (uniforme.cliente.elCliente().equals("Oficina")){
				uniforme.setColor(uniforme.cliente.getColor());
			}
		}	
	}

	@Override
	public Taller pasoPorElTaller() {
		return this;
	}

}
