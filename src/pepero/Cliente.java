package pepero;

import java.util.Collection;
import java.util.HashSet;

public abstract class Cliente {
	
	protected String nombreEmpresa;
	protected Color color;
	protected int valor;
	
	public Cliente (String _nombreEmpresa, Color _color, int _valor) {
		super();
		this.nombreEmpresa = _nombreEmpresa;
		this.color = _color;
		this.valor = _valor;
	}
	
	public String getNombreEmpresa(){
		return nombreEmpresa;
	}
	
	public Color getColor(){
		return color;
	}
	
	public int getValor(){
		return this.valor;
	}
	
	public boolean leyendaCorrecta(Uniforme uniforme){
		return this.getNombreEmpresa().equals(uniforme.getLeyenda());
	}
	
	public abstract boolean leGustaElColor(Uniforme uniforme);
	
	public abstract boolean suResistenciaEsSuficiente(Uniforme uniforme);
	
	public Collection <Uniforme> uniformesListos= new HashSet<>();
	
	public void agregarUniformeListo(Uniforme uniforme){
		this.uniformesListos.add(uniforme);
	}
	
	public void agregarUniformesListos(Collection <Uniforme> uniformesListosDelCentro){
		this.uniformesListos.addAll(uniformesListosDelCentro);
	}
	
	public Collection <Uniforme> getUniformesListos(){
		return this.uniformesListos;
	}
	
	public int valorTotalUniformesRecibidos(){
		return this.uniformesListos.stream()
		.mapToInt(v -> v.getCliente().getValor())
		.sum();
	}
	
	public abstract String elCliente();
	
	//public Cliente elCliente(){
		//return this;
	//}

	
}	
	
