package pepero;

public class Bordador extends Taller {

	@Override
	public boolean puedeTrabajarConUniforme(Uniforme uniforme) {
		return true;
	}

	@Override
	public void elTallerHaceLoqueTieneQueHacer(Uniforme uniforme) {
		if (this.puedeTrabajarConUniforme(uniforme)){
			uniforme.setLeyenda(uniforme.cliente.getNombreEmpresa());
		}	
	}

	@Override
	public Taller pasoPorElTaller() {
		return this;
	}

}
