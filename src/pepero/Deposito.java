package pepero;

public class Deposito extends Cliente{
	
	private static double resistenciaDeposito = 7;
	
	public Deposito (String _nombreEmpresa, Color _color, int _valor) {
		super (_nombreEmpresa, _color, _valor);
	}
	
	public double getResistenciaDeposito(){
		return resistenciaDeposito;
	}
	
	@Override
	public boolean leGustaElColor(Uniforme uniforme) {
		return (uniforme.getColor().equals(Color.NEGRO) || 
			uniforme.getColor().equals(Color.AZUL) ||
			uniforme.getColor().equals(Color.MARRON));
		}


	@Override
	public boolean suResistenciaEsSuficiente(Uniforme uniforme) {
		return getResistenciaDeposito() <= uniforme.getGradoResistenciaUniforme();
	}

	@Override
	public String elCliente() {
		return "Deposito";
	}

	
}	


//Necesitan uniformes de m�s de 7 pepines, 
// y que adem�s sean de uno de estos colores: negro, azul o marr�n. 
//El 7 puede cambiar por otro valor, pero siempre va a ser el mismo para todos los dep�sitos	

