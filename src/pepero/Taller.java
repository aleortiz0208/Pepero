package pepero;

public abstract class Taller {
	
	public abstract boolean puedeTrabajarConUniforme(Uniforme uniforme);
	
	public abstract void elTallerHaceLoqueTieneQueHacer(Uniforme uniforme);
	
	public abstract Taller pasoPorElTaller();

}	