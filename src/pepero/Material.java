package pepero;

public class Material {
	
	protected String material;
	protected int resistenciaMaterial;
	private int refuerzosMaterial;
	private int costurasExtrasMaximas;
	
	public Material (String _material, int _resistencia, int _costurasExtrasMaximas) {
		this.material = _material;
		this.resistenciaMaterial = _resistencia; 
		this.costurasExtrasMaximas = _costurasExtrasMaximas;
	}
	
	public String getMaterial(){ return this.material; }
	
	public int getResistencia(){ return this.resistenciaMaterial; }
	
	public int getRefuerzos(){ return refuerzosMaterial; }

	public int getCosturasExtrasMaximas(){ return costurasExtrasMaximas; }
	
}


